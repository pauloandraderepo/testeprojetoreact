import { observable } from 'mobx';

class MainStore {
	@observable view = 'login';
	@observable items = [
		{ id: 1, firstName: 'Mark', lastName: 'Otto', userName: '@mdo' },
		{ id: 2, firstName: 'Jacob', lastName: 'Thornton', userName: '@fat' },
		{ id: 3, firstName: 'Larry', lastName: 'The Bird', userName: '@twitter' },
];

  filterItems(filter) {
    return observable(this.items.filter((user) => {
      const firstName = user.firstName.toLowerCase();
      const lastName = user.lastName.toLowerCase();
      const userName = user.userName.toLowerCase();
      const queryFilter = filter.toLowerCase();
      return (firstName.includes(queryFilter) || lastName.includes(queryFilter) || userName.includes(queryFilter));
    }));
  }

  changeView(newView) {
    this.view = newView;
  }
}

export default MainStore;
