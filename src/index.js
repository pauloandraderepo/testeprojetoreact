import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'mobx-react';
import App from './components/App';
import MainStore from './model/MainStore';

render(<Provider mainStore={new MainStore()}><App /></Provider>,document.getElementById('root'));
