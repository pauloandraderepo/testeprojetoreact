import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Header extends Component {
  render() {
    return (
      <div className="login-header">
        <span className="panel-title white-text">{this.props.title}</span>
        <span
          className="glyphicon glyphicon-remove pull-right white-text"
          aria-hidden="true"
          style={{ cursor: 'pointer' }}
          onClick={() => this.props.onCloseWindow()}
        />
      </div>
    );
  }
}

Header.PropTypes = {
  title: PropTypes.string,
  onCloseWindow: PropTypes.func.isRequired,
};

export default Header;
