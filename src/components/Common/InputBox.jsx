import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputBox extends Component {
  callbackInputValue() {
    if (this.props.handleInputValue != null) {
      this.props.handleInputValue(this.textInput.value);
    }
  }
  render() {
    const glyphiconClass = `glyphicon glyphicon-${this.props.iconName} input-icon`;
    return (
      <div className="form-group input-container">
        <input
          className="form-control input-box"
          placeholder={this.props.placeholder}
          type={this.props.type}
          ref={(input) => { this.textInput = input; }}
          onChange={() => this.callbackInputValue()}
        />
        <span className={glyphiconClass} aria-hidden="true" />
      </div>
    );
  }
}

InputBox.propTypes = {
  iconName: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  handleInputValue: PropTypes.func
};

export default InputBox;
