import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import Header from '../Common/Header';
import ItemsTable from './ItemsTable';
import InputBox from '../Common/InputBox';

@inject('mainStore') @observer
class SearchItems extends Component {
  constructor(props) {
    super(props);
    this.state = { items: props.mainStore.items };
  }
  filterItems(value) {
    if (value == null || value.length === 0) {
      this.setState({ items: this.props.mainStore.items });
    } else {
      this.setState({ items: this.props.mainStore.filterItems(value) });
    }
  }
  render() {
    return (
      <div className="container login-container">
        <div className="col xs-3 col-md-3 col-sm-12 col-xs-12" />
        <div className="col xs-6 col-md-6 col-sm-12 col-xs-12">
          <div className="login-panel panel-default">
            <Header title="Search Items" onCloseWindow={() => this.props.mainStore.changeView('close-all')} />
            <div className="panel-body">
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sd-12 col-xs-12">
                  <InputBox placeholder="Filter" iconName="search" type="text" handleInputValue={(value) => this.filterItems(value)} />
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sd-12 col-xs-12">
                  <ItemsTable items={this.state.items} />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col xs-3 col-md-3 col-sm-12 col-xs-12" />
      </div>
    );
  }
}

SearchItems.PropTypes = {
  mainStore: PropTypes.objectOf(PropTypes.shape({
    changeView: PropTypes.func,
    filtrItems: PropTypes.func,
    items: PropTypes.object
  }))
};

export default SearchItems;
