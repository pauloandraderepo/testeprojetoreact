import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ItemsTable extends Component {
  render() {
    return (
      <div className="table-responsive" style={{ margin: '15px' }}>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>UserName</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.items.map((user) => (
                <tr key={user.id}>
                  <td>{user.firstName}</td>
                  <td>{user.lastName}</td>
                  <td>{user.userName}</td>
                </tr>
               ))
            }
          </tbody>
        </table>
      </div>
    );
  }
}

ItemsTable.propTypes = {
  items: PropTypes.objectOf(PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    userName: PropTypes.string
  }))
};

export default ItemsTable;
