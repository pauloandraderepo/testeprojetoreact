import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import Header from '../Common/Header';
import InputBox from '../Common/InputBox';
import Buttons from './Buttons';

@inject('mainStore') @observer
class Login extends Component {
  render() {
    return (
      <div className="container login-container">
        <div className="col xs-3 col-md-3 col-sm-12 col-xs-12" />
        <div className="col xs-6 col-md-6 col-sm-12 col-xs-12">
          <div className="login-panel panel-default">
            <Header title="Login Form" onCloseWindow={() => this.props.mainStore.changeView('close-all')} />
            <div className="panel-body">
              <form acceptCharset="UTF-8" role="form">
                <fieldset>
                  <div className="col-md-12 col-sm-12 col-xs-12">
                    <InputBox placeholder="Username" iconName="user" type="text" />
                  </div>
                  <div className="col-md-12 col-sm-12 col-xs-12">
                    <InputBox placeholder="Password" iconName="lock" type="password" />
                  </div>
                  <Buttons />
                </fieldset>
              </form>
            </div>
          </div>
        </div>
        <div className="col xs-3 col-md-3 col-sm-12 col-xs-12" />
      </div>
    );
  }
}

Login.PropTypes = {
  mainStore: PropTypes.objectOf(PropTypes.shape({
    changeView: PropTypes.func.isRequired,
  })),
};

export default Login;
