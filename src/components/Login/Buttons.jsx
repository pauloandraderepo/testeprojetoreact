import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';

@inject('mainStore') @observer
class Buttons extends Component {
  render() {
    return (
      <div className="col-lg-12 col-md-12 col-sd-12 col-xs-12">
        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
          <a
            className="btn btn-primary signin-btn white-text"
            href="#"
            role="button"
            onClick={() => this.props.mainStore.changeView('search-items')}
          >Sign In</a>
        </div>
        <div className="col-lg-8 col-md-8 col-sm-12 col-xs-12" style={{ paddingTop: '3%' }}>
          <a className="lostpassword-link" href="#">Lost your Password?</a>
        </div>
      </div>
    );
  }
}

Buttons.PropTypes = {
  mainStore: PropTypes.objectOf(PropTypes.shape({
    changeView: PropTypes.func.isRequired,
  })),
};

export default Buttons;
