import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import Login from './Login/Login';
import SearchItems from './SearchItems/SearchItems';

@inject('mainStore') @observer
class App extends Component {
  render() {
    let currentView = null;
    switch (this.props.mainStore.view) {
      case 'login':
        currentView = <Login />;
        break;
      case 'close-all':
        currentView = <br />;
        break;
      case 'search-items':
        currentView = <SearchItems />;
        break;
      default:
        currentView = <Login />;
        break;
    }

    return (
      <div className="background">
        { currentView }
      </div>
    );
  }
}

App.PropTypes = {
  mainStore: PropTypes.objectOf(PropTypes.shape({
    view: PropTypes.string,
  }))
};

export default App;
