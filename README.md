# Exemplo de Projeto ReactJS com ES6 e Webpack

Tecnologias utilizadas:
* [React](https://github.com/facebook/react)
* [Babel 6](http://babeljs.io)
* [Webpack](http://webpack.github.io) para o bundling
* [Webpack Dev Server](http://webpack.github.io/docs/webpack-dev-server.html)
* [React Transform](https://github.com/gaearon/react-transform-hmr)
* [MobX](https://github.com/mobxjs/mobx)

### Uso

```
npm install
npm start
url http://localhost:8080
```

### Validacao do código
O projeto utiliza o ESLint para validar sintaxes, formatação do código, etc.

```
npm run lint
```